require 'httparty'

require_relative 'base_adapter'
require_relative '../ui'

module Gitlab
  module Triage
    module NetworkAdapters
      class HttpartyAdapter < BaseAdapter
        def get(token, url)
          response = HTTParty.get(
            url,
            headers: {
              'Content-type' => 'application/json',
              'PRIVATE-TOKEN' => token
            }
          )

          raise_on_unauthorized_error!(response)

          {
            more_pages: (response.headers["x-next-page"].to_s != ""),
            next_page_url: url + "&page=#{response.headers['x-next-page']}",
            results: response.parsed_response,
            ratelimit_remaining: response.headers["ratelimit-remaining"].to_i,
            ratelimit_reset_at: Time.at(response.headers["ratelimit-reset"].to_i)
          }
        end

        def post(token, url, body)
          response = HTTParty.post(
            url,
            body: body.to_json,
            headers: {
              'Content-type' => 'application/json',
              'PRIVATE-TOKEN' => token
            }
          )

          raise_on_unauthorized_error!(response)

          {
            results: response.parsed_response,
            ratelimit_remaining: response.headers["ratelimit-remaining"].to_i,
            ratelimit_reset_at: Time.at(response.headers["ratelimit-reset"].to_i)
          }
        end

        private

        def raise_on_unauthorized_error!(response)
          return unless response.response.is_a?(Net::HTTPUnauthorized)

          puts Gitlab::Triage::UI.debug response.inspect if options.debug
          raise 'The provided token is unauthorized!'
        end
      end
    end
  end
end
