# frozen_string_literal: true

require_relative 'base'

module Gitlab
  module Triage
    module Action
      class Summarize < Base
        class Dry < Summarize
          private

          def perform
            puts "The following issue would be created for the rule **#{policy.name}**:\n\n"
            puts ">>>"
            puts "* Title: #{issue.title}"
            puts "* Description: #{issue.description}"
            puts ">>>"
          end
        end

        def act
          perform if issue.valid?
        end

        private

        def perform
          network.post_api(post_issue_url, post_issue_body)
        end

        def issue
          @issue ||= policy.build_issue
        end

        def post_issue_url
          # POST /projects/:id/issues
          # https://docs.gitlab.com/ee/api/issues.html#new-issue
          post_url = UrlBuilders::UrlBuilder.new(
            network_options: network.options,
            source_id: network.options.source_id,
            resource_type: 'issues'
          ).build

          puts Gitlab::Triage::UI.debug "post_issue_url: #{post_url}" if network.options.debug

          post_url
        end

        def post_issue_body
          {
            title: issue.title,
            description: issue.description
          }
        end
      end
    end
  end
end
