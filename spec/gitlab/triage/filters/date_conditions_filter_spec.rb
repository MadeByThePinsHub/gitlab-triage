require 'spec_helper'

require 'gitlab/triage/filters/date_conditions_filter'

describe Gitlab::Triage::Filters::DateConditionsFilter do
  let(:created_at) { Date.new(2016, 1, 31) }
  let(:updated_at) { Date.new(2017, 1, 1) }

  let(:resource) do
    {
      created_at: created_at,
      updated_at: updated_at
    }
  end
  let(:condition) do
    {
      attribute: 'updated_at',
      condition: 'older_than',
      interval_type: 'months',
      interval: 3
    }
  end

  subject { described_class.new(resource, condition) }

  it_behaves_like 'a filter'

  describe '#resource_value' do
    it 'has the correct value for updated_at attribute' do
      expect(subject.resource_value).to eq(updated_at)
    end

    it 'has the correct value for the created_at attribute' do
      filter = described_class.new(resource, condition.merge(attribute: 'created_at'))
      expect(filter.resource_value).to eq(created_at)
    end
  end

  describe '#condition_value' do
    it 'has the correct value for comparison' do
      expect(subject.condition_value).to eq(3.months.ago.to_date)
    end
  end

  describe '#calculate' do
    it 'calculates true given correct condition' do
      expect(subject.calculate).to eq(true)
    end

    it 'calculate false given wrong condition' do
      filter = described_class.new(resource, condition.merge(condition: 'newer_than'))
      expect(filter.calculate).to eq(false)
    end
  end
end
