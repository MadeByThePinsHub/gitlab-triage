require 'spec_helper'

require 'gitlab/triage/filters/assignee_member_conditions_filter'

describe Gitlab::Triage::Filters::AssigneeMemberConditionsFilter do
  let(:type) { :assignee }

  it_behaves_like 'a member filter'
end
